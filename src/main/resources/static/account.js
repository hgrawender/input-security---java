window.onload = function() {

    var last_tds = document.getElementsByClassName('last_td');

    for (var i = 0; i < last_tds.length; i++) {
        if (parseFloat(last_tds[i].innerHTML) > 0) {
            var tr = last_tds[i].parentElement
            $(tr).addClass('table-success');
            var third_el = last_tds[i].parentElement.childNodes[5];
            $(third_el).remove();
        }
        else {
            var fourth_el = last_tds[i].parentElement.childNodes[7];
            $(fourth_el).remove();
        }
    }
};