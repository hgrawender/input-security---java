
    //onload event
    window.onload = function(){
    //get elements  
    var name = document.getElementById("name");
    var nameError = document.getElementById("nameError");
    var postCode = document.getElementById('postCode');
    var postCodeError = document.getElementById('postCodeError')
    var city = document.getElementById("city"); 
    var cityError = document.getElementById("cityError"); 
    var accountNoTo = document.getElementById("accountNoTo");
    var accountNoToError = document.getElementById("accountNoToError");
    var amount = document.getElementById("amount");
    var amountError = document.getElementById("amountError");

    var reChar = new RegExp("([a-ząćęłżźóśń A-ZĄĆĘŁŻŹÓŚŃ])$");
    var reAccount = new RegExp("([0-9]{16})$");
    var rePost = new RegExp("([0-9]{2}[-][0-9]{3})$");
    
    name.oninput = function(){
        $(nameError).addClass('hidden');
        $(name).removeClass('form-control-red');
        if(!reChar.test(this.value)){
            $(nameError).removeClass('hidden');
            $(name).addClass('form-control-red');
            if(!this.value){
                $(nameError).addClass('hidden');
                $(name).removeClass('form-control-red');
            }
        }
    };

    city.oninput = function(){
        $(cityError).addClass('hidden');
        $(city).removeClass('form-control-red');
        if(!reChar.test(this.value)){
            $(cityError).removeClass('hidden');
            $(city).addClass('form-control-red');
            if(!this.value){
                $(cityError).addClass('hidden');
                $(city).removeClass('form-control-red');
            }
        }
        
    };

    postCode.oninput = function(){
        $(postCodeError).addClass('hidden');
        $(postCode).removeClass('form-control-red');
        if(!rePost.test(this.value)){
            $(postCodeError).removeClass('hidden');
            $(postCode).addClass('form-control-red');
            if(!this.value){
                $(postCodeError).addClass('hidden');
                $(postCode).removeClass('form-control-red');
            }
        }

    };

    accountNoTo.oninput = function(){
        $(accountNoToError).addClass('hidden');
        $(accountNoTo).removeClass('form-control-red');
        if(!reAccount.test(this.value)){
            $(accountNoToError).removeClass('hidden');
            $(accountNoTo).addClass('form-control-red');
            if(!this.value){
                $(accountNoToError).addClass('hidden');
                $(accountNoTo).removeClass('form-control-red');
            }
        }
    };

    accountNoTo.onpaste = accountPasted;

    function accountPasted() {
        confirm("You want to paste the account number. Please, verify if it is correct!");
    };

    var cash = document.getElementById('cash').innerHTML;
    amount.oninput = function(){
        $(amountError).addClass('hidden');
        $(amount).removeClass('form-control-red');
        if(this.value > parseFloat(cash)){
            $(amountError).removeClass('hidden');
            $(amount).addClass('form-control-red');
            if(!this.value){
                $(amountError).addClass('hidden');
            }
        }
    };

};
