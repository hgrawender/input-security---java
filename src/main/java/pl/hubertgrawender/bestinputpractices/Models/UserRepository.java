package pl.hubertgrawender.bestinputpractices.Models;

    import org.springframework.data.jpa.repository.JpaRepository;
    import org.springframework.data.jpa.repository.Query;
    import org.springframework.data.repository.query.Param;
    import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository  extends JpaRepository<User, Long> {
        User findByLogin(String login);
        User findById(long id);
        User findByAccountNo(String accountNo);

    @Query("select count(id) from User where id = :Uid and cash >= :amount")
    Long isSufficientFunds(@Param("Uid") long Uid, @Param("amount") Double amount);

//    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Company c WHERE c.name = :companyName")
//    boolean existsByName(@Param("companyName") String companyName);
    }

