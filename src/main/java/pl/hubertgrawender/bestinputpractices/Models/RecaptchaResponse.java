package pl.hubertgrawender.bestinputpractices.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecaptchaResponse {

    private boolean success;
    private double score;
    private String challenge_ts;
    private List<String> errorCodes;

}