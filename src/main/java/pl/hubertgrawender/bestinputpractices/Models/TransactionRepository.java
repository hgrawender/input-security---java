package pl.hubertgrawender.bestinputpractices.Models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Transaction findById(long id);
    List<Transaction> findAllByOrderByDateDesc();

//    @Query(value = "SELECT account_no_to FROM transaction where id = 1", nativeQuery = true)
//    List<String> getTitleQuery();
    }

