package pl.hubertgrawender.bestinputpractices.Models;

import lombok.Data;
import pl.hubertgrawender.bestinputpractices.Validators.AmountConstraint;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;


@Data
@Entity
@Table(name = "Transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    //@Pattern(regexp="^[0-9]+((\\.|\\,)[0-9]{1,2})?$", message = "Amount of cash should be in form 123.23 or 123,23 or 123")
    @Column(scale=2)
    @AmountConstraint
    private Double amount;

    @Pattern(regexp="^[0-9]{16}$", message = "Account number have to consist only 16 digits")
    private String accountNoFrom;

    @Pattern(regexp="^[0-9]{16}$", message = "Account number have to consist only 16 digits")
    private String accountNoTo;

    private String title;
    private String name;
    private String street;

    @Pattern(regexp="^[0-9]{2}-[0-9]{3}$", message = "Post code should be in form XX-XXX")
    private String postCode;

    private String city;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATE")
    private java.util.Calendar date;
}
