package pl.hubertgrawender.bestinputpractices.Models;

import lombok.Data;
import pl.hubertgrawender.bestinputpractices.Validators.AmountConstraint;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Data
@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String login;
    private String password;

    @Pattern(regexp="^[0-9]{16}$", message = "Account number have to consist only 16 digits")
    private String accountNo;

    @Column(scale=2)
    @AmountConstraint
    private Double cash;

    private String name;
    private String street;

    @Pattern(regexp="^[0-9]{2}-[0-9]{3}$", message = "Post code should be like XX-XXX")
    private String postCode;

    private String city;
}
