package pl.hubertgrawender.bestinputpractices.Utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import pl.hubertgrawender.bestinputpractices.Models.RecaptchaResponse;

import java.util.regex.Pattern;

@Service
public class RecaptchaService {

    @Value("${google.recaptcha.key.secret}")
    private String secret;

    @Value("${google.recaptcha.threshold}")
    private double threshold;

    private static final Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

    public boolean verify(final String response) {

        if (!responseSanityCheck(response)) {
            System.out.println("Sanity failed!");
           return false;
        }

        RestTemplate restTemplate = new RestTemplate();
        RecaptchaResponse recaptchaResponse = restTemplate.getForObject(String.format("https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s", secret, response), RecaptchaResponse.class);

        try {
            if(recaptchaResponse.isSuccess()){
                double score = recaptchaResponse.getScore();
                System.out.println("score: "+score);
                if (Math.abs(score-threshold) >= 0.000001) return true;
                else return false;
            }
        } catch (Exception e){ }
        return false;
    }

    private boolean responseSanityCheck(final String response) {
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }


}
