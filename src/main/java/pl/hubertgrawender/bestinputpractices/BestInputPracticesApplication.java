package pl.hubertgrawender.bestinputpractices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BestInputPracticesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BestInputPracticesApplication.class, args);
	}
}
