package pl.hubertgrawender.bestinputpractices.Validators;

import org.hibernate.annotations.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Constraint(validatedBy = AmountValidator.class)
//@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AmountConstraint {
    String message() default "Amount of cash should be in form 123.23 or 123,23 or 123";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
