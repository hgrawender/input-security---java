package pl.hubertgrawender.bestinputpractices.Validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AmountValidator implements
        ConstraintValidator<AmountConstraint, Double> {

        @Override
        public void initialize(AmountConstraint contactNumber) {
        }

        @Override
        public boolean isValid(Double amountField,
                               ConstraintValidatorContext cxt) {
            return amountField != null && amountField.toString().matches("^[0-9]+((\\.|\\,)[0-9]{1,2})?$")
                    && amountField > 0;
        }

    }
