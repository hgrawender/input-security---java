package pl.hubertgrawender.bestinputpractices.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.hubertgrawender.bestinputpractices.Models.Transaction;
import pl.hubertgrawender.bestinputpractices.Models.User;
import pl.hubertgrawender.bestinputpractices.Models.UserRepository;
import pl.hubertgrawender.bestinputpractices.Utils.RecaptchaService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.IOException;

import static org.springframework.web.util.HtmlUtils.htmlEscape;

@Controller
public class LoginController{

    @Autowired
    private RecaptchaService recaptchaService;

    private UserRepository userRepository;
    @Autowired(required = true)
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @GetMapping("/login")
    public String showForm(User user) {
        return "login";
    }

    @PostMapping(value = "/login")
    public String getLogin(User userForm, BindingResult bindingResult, /*User user,*/ HttpServletRequest req, HttpServletResponse res) {
        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) { // 1.
                String fieldErrors = ((FieldError) error).getField(); // 2.
                System.out.println(fieldErrors);
            }
        }

        String login = userForm.getLogin();
        String loginEsc = htmlEscape(login);

        HttpSession session = req.getSession();
        session.setAttribute("login", loginEsc);


        System.out.println(session.getAttribute("login").toString());
        //model.addAttribute("name", "Ania");
        //model.addAttribute("name", nameForm);
        //return "redirect:/results";
        return "loginStep2";
    }

    @PostMapping(value = "/loginStep2")
    public String getLogin2(@Valid User userForm, BindingResult bindingResult, Transaction transaction, HttpServletRequest req, HttpServletResponse res, Model model) throws IOException {
        HttpSession session = req.getSession();
        if (bindingResult.hasErrors() || (null == session.getAttribute("login"))) {
            //return "login";
        }

        String recaptchaResponse = req.getParameter("recaptchaResponse");
        boolean verified = recaptchaService.verify(recaptchaResponse);
        if(!verified){
            model.addAttribute("badRecaptcha", true);
            return "login";
        }



        String login = session.getAttribute("login").toString();
        String password = userForm.getPassword();


        User userDB = userRepository.findByLogin(login);

        if (userDB != null) {
            System.out.println("user.getPassword(): "+userDB.getPassword());
            System.out.println("password: "+password);

            if(userDB.getPassword().equals(password)){
                session.setAttribute("loggedIn", true);
                session.setAttribute("id", userDB.getId());
            } else{
                model.addAttribute("badCredentials", true);
                return "login";
            }

        } else{
            model.addAttribute("badCredentials", true);
            return "login";
        }
        res.sendRedirect("/account");
        return "account";
    }


    @RequestMapping("/403")
    public String show403() {
        return "403";
    }

}
