package pl.hubertgrawender.bestinputpractices.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import pl.hubertgrawender.bestinputpractices.Models.User;
import pl.hubertgrawender.bestinputpractices.Models.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LogOutController {

    @GetMapping("/logout")
    public String logout(HttpServletRequest req, HttpServletResponse res) {

        req.removeAttribute("id");
        return "logout";
    }
}
