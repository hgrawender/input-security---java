package pl.hubertgrawender.bestinputpractices.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import pl.hubertgrawender.bestinputpractices.Models.Transaction;
import pl.hubertgrawender.bestinputpractices.Models.TransactionRepository;
import pl.hubertgrawender.bestinputpractices.Models.User;
import pl.hubertgrawender.bestinputpractices.Models.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Controller
public class AccountController {

    private UserRepository userRepository;
    @Autowired(required = true)
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private TransactionRepository transactionRepository;
    @Autowired(required = true)
    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @GetMapping("/account")
    public String account(HttpServletRequest req, HttpServletResponse res, ModelMap map) {
        long id = Long.parseLong(String.valueOf(req.getSession().getAttribute("id")));
        User user = userRepository.findById(id);

        map.addAttribute("user", user);

        List<Transaction> tHistory = transactionRepository.findAllByOrderByDateDesc();
        for(Transaction t : tHistory){
            t.getAccountNoFrom();
            if(t.getAccountNoFrom().equals(user.getAccountNo())) t.setAmount(0 - t.getAmount());
        }

        map.addAttribute("tHistory", transactionRepository.findAllByOrderByDateDesc());
        return "account";
    }
}
