package pl.hubertgrawender.bestinputpractices.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.hubertgrawender.bestinputpractices.Models.Transaction;
import pl.hubertgrawender.bestinputpractices.Models.TransactionRepository;
import pl.hubertgrawender.bestinputpractices.Models.User;
import pl.hubertgrawender.bestinputpractices.Models.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import static org.springframework.web.util.HtmlUtils.htmlEscape;


@Controller
public class TransactionController {

    private UserRepository userRepository;
    @Autowired(required = true)
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private TransactionRepository transactionRepository;
    @Autowired(required = true)
    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }


    @GetMapping("/transaction")
    public String showForm(Transaction transaction, HttpServletRequest req, HttpServletResponse res, ModelMap map) {

        long id = Long.parseLong(String.valueOf(req.getSession().getAttribute("id")));
        User user = userRepository.findById(id);

        map.addAttribute("user", user);
        return "transaction";
    }

    @PostMapping(value = "/transaction")
    public String doTransaction(@Valid Transaction tForm, BindingResult bindingResult, HttpServletRequest req, HttpServletResponse res,  ModelMap map) throws IOException {
        long id = Long.parseLong(String.valueOf(req.getSession().getAttribute("id")));
        User user = userRepository.findById(id);
        map.addAttribute("user", user);

        if (bindingResult.hasErrors()) return "transaction";

        if(userRepository.isSufficientFunds(id, tForm.getAmount()) < 1){
            map.addAttribute("error", "You have insufficient amount of money!");
            return "transaction";
        }


        User userTo = userRepository.findByAccountNo(tForm.getAccountNoTo());
        if(userTo == null){
            map.addAttribute("error", "This account number does not exist!");
            return "transaction";
        }
        double newCash = userTo.getCash() + tForm.getAmount();

        //Adresat
        userTo.setCash(newCash);
        userRepository.save(userTo);

        //Nadawca
        user.setCash(user.getCash()-tForm.getAmount());
        userRepository.save(user);

        tForm.setAccountNoFrom(user.getAccountNo());
        Calendar now = Calendar.getInstance();
        tForm.setDate(now);
        transactionRepository.save(tForm);

//        String login = userForm.getLogin();
//        String loginEsc = htmlEscape(login);

        map.addAttribute("success", "Money has been sent successfully");
        return "transaction";
    }
}
