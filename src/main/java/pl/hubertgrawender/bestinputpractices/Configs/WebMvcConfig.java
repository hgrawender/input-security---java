package pl.hubertgrawender.bestinputpractices.Configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //Interceptor apply to all URLs.
        registry.addInterceptor(new Interceptor())
                .addPathPatterns("/transaction")
                .addPathPatterns("/logout")
                .addPathPatterns("/account");
                //.excludePathPatterns("/login")

//        registry.addInterceptor(new OldLoginInterceptor())//
//                .addPathPatterns("/admin/oldLogin");
//                .excludePathPatterns("/admin/oldLogin");
    }
}